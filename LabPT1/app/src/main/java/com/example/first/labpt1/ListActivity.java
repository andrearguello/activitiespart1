package com.example.first.labpt1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    String[] array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        array= new String[]{"Hello", "World"};
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        final ListView list= (ListView)findViewById(R.id.ListView);
        ArrayAdapter<String> adapt= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,array);
        list.setAdapter(adapt);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int index=i; //Position in the list
                String data = (String) list.getItemAtPosition(index);
                Intent intent = new Intent(ListActivity.this, SecondView.class);
                intent.putExtra("StringToSend", data);
                startActivity(intent);
            }
        });
    }
}
