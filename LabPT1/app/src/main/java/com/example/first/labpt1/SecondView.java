package com.example.first.labpt1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondView extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_view);

        Bundle extras = getIntent().getExtras();
        String toPrint =  extras.getString("StringToSend");
        textView = (TextView) findViewById(R.id.textView);
        textView.setText(toPrint);
    }
}
